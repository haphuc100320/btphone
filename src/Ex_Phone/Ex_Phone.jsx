import React, { Component } from 'react'
import { phoneArr } from './dataPhone'
import PhoneDetail from './PhoneDetail'
import PhoneList from './PhoneList'

export default class Ex_Phone extends Component {
    state = {
        phoneArr: phoneArr,
        phoneDetail: phoneArr[0]
    }
    renderHand=(sp) => { 
        this.setState({phoneDetail:sp})
        
     }
  render() {
    return (
      <div className='container py-5'>
        <PhoneList renderOnClick={this.renderHand} data={this.state.phoneArr}/>
        <PhoneDetail phoneDetail={this.state.phoneDetail}/>
      </div>
    )
  }
}
