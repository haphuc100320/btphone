import React, { Component } from 'react'

export default class PhoneDetail extends Component {
  render() {
    let {
        maSP,
        tenSP,
        manHinh,
        heDieuHanh,
        giaBan,
        hinhAnh,
    }
    =this.props.phoneDetail
    return (
      <div>
        <div>
                <div className='row mt-5'>
                    <div className="col-4">
                        <img src={hinhAnh} className="w-100" alt="" />
                    </div>
                    <div className="col-8 display-4 text-left">
                        <p>Tên Sản Phẩm {tenSP} </p>
                        <p>Màn Hình {manHinh} </p>
                        <p>Hệ Điều Hành{heDieuHanh} </p>
                    </div>
                </div>
            </div>
      </div>
    )
  }
}
