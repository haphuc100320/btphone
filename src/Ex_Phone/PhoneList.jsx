import React, { Component } from 'react'
import PhoneItem from './PhoneItem'

export default class PhoneList extends Component {
    renderPhone=() => { 
        return this.props.data.map((item) => { 
            return <PhoneItem renderClick={this.props.renderOnClick} data={item}/>
         })
     }
     
  render() {
    
    return (
      <div className='row'>
        {this.renderPhone()}
      </div>
    )
  }
}
